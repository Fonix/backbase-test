//
//  FileReader.swift
//  Backbase Test
//
//  Created by Jacques Questiaux on 2020/09/15.
//  Copyright © 2020 Jacques Questiaux. All rights reserved.
//

import UIKit

struct FileReader {
  
  var bundle: Bundle
  
  init(bundle: Bundle = Bundle.main) {
    self.bundle = bundle
  }
  
  func readData<T: Decodable>(from assetFile: String, forType: T.Type) throws -> T  {
    let asset = NSDataAsset(name: assetFile, bundle: bundle)
    let decoder = JSONDecoder()
    return try decoder.decode(T.self, from: asset!.data)
  }
}
