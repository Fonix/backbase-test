//
//  CityMapViewController.swift
//  Backbase Test
//
//  Created by Jacques Questiaux on 2020/09/19.
//  Copyright © 2020 Jacques Questiaux. All rights reserved.
//

import UIKit
import MapKit

class CityMapViewController: UIViewController {

  @IBOutlet weak var mapView: MKMapView!
  @IBOutlet weak var backButtonView: UIView!
  
  var city: City?
  
  override func viewDidLoad() {
      super.viewDidLoad()
    
    backButtonView.isHidden = UIDevice.current.userInterfaceIdiom == .pad
    backButtonView.layer.cornerRadius = 10
    
    setAnnotationFor(city: city)
  }

  @IBAction func backButtonPressed(_ sender: UIButton) {
    navigationController?.popViewController(animated: true)
  }
  
  func setCity(_ city: City) {
    self.city = city
    setAnnotationFor(city: city)
  }
  
  func setAnnotationFor(city: City?) {
    if let city = city, mapView != nil {
      mapView.removeAnnotations(mapView.annotations)
      
      let annotation = MKPointAnnotation()
      annotation.coordinate = CLLocationCoordinate2D(latitude: city.coordinates.lat, longitude: city.coordinates.lon)
      mapView.addAnnotation(annotation)
      
      if UIDevice.current.userInterfaceIdiom == .pad {
        // Animate map movement for ipad, since map is always visible
        let region = MKCoordinateRegion(center: annotation.coordinate, span: MKCoordinateSpan(latitudeDelta: 1, longitudeDelta: 1))
        mapView.setRegion(region, animated: true)
      } else {
        mapView.setCenter(annotation.coordinate, animated: false)
      }
    }
  }
}
