//
//  CityViewModel.swift
//  Backbase Test
//
//  Created by Jacques Questiaux on 2020/09/17.
//  Copyright © 2020 Jacques Questiaux. All rights reserved.
//

import Foundation

class CityViewModel {
  
  private var cities: [City] = []
  private let dataSource: CityDataSource
  
  init(dataSource: CityDataSource = CityDataSource()) {
    self.dataSource = dataSource
  }
  
  /**
   Loads the city data from the data source. This is a synchronous operation.
   
   - Parameters:
    - filename: The file in which you want to load the data from.
   */
  func loadCityData(from filename: String = "cities") {
    cities = (try? dataSource.sortedCityData(from: filename)) ?? []
  }
  
  /**
   Gets a list of cities in alphabetical order (case insensitive) that match the specified `prefix`.
   
    - Parameters:
      - prefix: The string to match the first part of the city name.
   
   - Returns: A list of cities whose name starts with the prefix specified.
   */
  func getCities(withPrefix prefix: String? = nil) -> [City] {
    if let prefix = prefix, prefix.count > 0 {
      return findCities(startingWith: prefix, from: cities)
    } else {
      return cities
    }
  }
  
  // MARK: - City list filtering
  
  private func findCities(startingWith prefix: String,
                          from cities: [City]) -> [City] {
    
    var firstInstanceOfPrefix: Int?
    var lastInstanceOfPrefix: Int?
    
    let sempahore = DispatchSemaphore(value: 0)
    
    // Split search of first and last instance of prefix onto different threads
    // since they are completely independant
    DispatchQueue.global().async {
      firstInstanceOfPrefix = self.findCities(startingWith: prefix,
                                              from: cities,
                                              splitPoint: cities.count/2,
                                              lowerBoundary: 0,
                                              upperBoundary: cities.count,
                                              searchDirection: .lower)
      
      sempahore.signal()
    }
    
    DispatchQueue.global().async {
      lastInstanceOfPrefix = self.findCities(startingWith: prefix,
                                             from: cities,
                                             splitPoint: cities.count/2,
                                             lowerBoundary: 0,
                                             upperBoundary: cities.count,
                                             searchDirection: .upper)
      sempahore.signal()
    }
    
    sempahore.wait()
    sempahore.wait()
    
    if let first = firstInstanceOfPrefix, let last = lastInstanceOfPrefix {
      return Array(cities[first...last])
    } else {
      return []
    }
  }
  
  private enum Direction {
    case lower
    case upper
  }
  
  /**
   Searches for the subset of cities with name starting with the `prefix` parameter.
   
   This uses a divide and conquer strategy to find the index in the city array of the
   lower and upper bound of where the cities thats name start with the prefix.
   
   Assumes the `cities` list is sorted by lowercased name (to allow for case insensitive searching).
   
    - Parameters:
      - prefix: The prefix of the name of the cities you are searching for.
      - cities: The list of sorted cities.
      - splitPoint: The current position where we are going to divide the search,
                    should always be the index halfway between the upper and lower boundary.
      - lowerBoundary: The lowest point where cities with the prefix do not exist.
      - upperBoundary: The highest point where cities with the prefix do not exist.
      - searchDirection: Determines if we are looking for the lower or upper bound.
   */
  private func findCities(startingWith prefix: String,
                          from cities: [City],
                          splitPoint: Int,
                          lowerBoundary: Int,
                          upperBoundary: Int,
                          searchDirection: Direction) -> Int? {
    
    let prefix = prefix.lowercased()
    
    if cities.count == 0 {
      return nil
    }
    
    var upperBoundary = upperBoundary
    var lowerBoundary = lowerBoundary
    let splitPoint = clampSplitPoint(splitPoint, cities: cities)
    // Get city at split point and get prefix of the name
    let cityPrefix = cities[splitPoint].name[0..<prefix.count].lowercased()
    let newSplitPoint: Int
    
    // If the city prefix and the specified prefix dont match, and our boundaries have collapsed together,
    // then the prefix cannot be found in the list of cities provided
    if cityPrefix != prefix && upperBoundary - lowerBoundary <= 1 {
      return nil
    }
    
    // Compare the specified prefix against the split point city's prefix
    switch cityPrefix.compare(prefix) {
    
    // If prefixes are alphabetically equal
    case .orderedSame:
      let index: Int
      let boundary: Int
      let comparison: ComparisonResult
      // If we are looking for the lower bound
      if searchDirection == .lower {
        // Get index of city lower in the array
        index = splitPoint - 1 > 0 ? splitPoint - 1 : 0
        boundary = lowerBoundary
        comparison = .orderedAscending // If the city below is alphabetically equal or greater
        
      } else { // Else if we are looking for the higher bound
        // Get index of city higher in the array
        index = splitPoint + 1 < cities.count ? splitPoint + 1 : cities.count - 1
        boundary = upperBoundary
        comparison = .orderedDescending // If the city above is alphabetically equal or less
      }
      
      // Get city name prefix that is the same length as our search prefix at the index
      let c = cities[index].name[0..<prefix.count].lowercased()
      if c.compare(prefix) != comparison {
        // Make new split point half way between the current split point and the boundary
        newSplitPoint = Int(Double(splitPoint + boundary) / 2.0)
        // Do not adjust the boundary as we have landed in the middle of the set we are looking for
      } else {
        // Else we have found our lower boundary
        return splitPoint
      }
      
      // If the split point has landed at the edges of the array,
      // then the prefix boundary is at the edge of the array and we can stop searching
      if newSplitPoint <= 0 || newSplitPoint >= cities.count - 1 {
        return clampSplitPoint(newSplitPoint, cities: cities)
      }
    
    // If prefix is alphabetically lower
    case .orderedDescending:
      // Make new split point half way between the current split point and the lower boundary
      newSplitPoint = Int(Double(splitPoint + lowerBoundary) / 2.0)
      // Set the current split point to be the upper boundary, as we do not need to search higher than this point
      upperBoundary = splitPoint
      
    // If prefix is alphabetically higher
    case .orderedAscending:
      // Make new split point half way between the current split point and the upper boundary
      newSplitPoint = Int(Double(splitPoint + upperBoundary) / 2.0)
      // Set the current split point to be the lower boundary, as we do not need to search lower than this point
      lowerBoundary = splitPoint
    }
    
    // If we have not found the boundary, search again with the new split point and potentially smaller boundaries
    return findCities(startingWith: prefix,
                      from: cities,
                      splitPoint: newSplitPoint,
                      lowerBoundary: lowerBoundary,
                      upperBoundary: upperBoundary,
                      searchDirection: searchDirection)
  }
  
  // Make sure the split point is clamped to the bounds of the array,
  // because in some edge cases it can jump slightly out
  private func clampSplitPoint(_ splitPoint: Int, cities: [City]) -> Int {
    return splitPoint < 0 ? 0 : splitPoint >= cities.count ? cities.count - 1 : splitPoint
  }
}
