//
//  CityListViewController.swift
//  Backbase Test
//
//  Created by Jacques Questiaux on 2020/09/15.
//  Copyright © 2020 Jacques Questiaux. All rights reserved.
//

import UIKit

class CityListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UITextFieldDelegate {

  @IBOutlet weak var loadingView: UIView!
  @IBOutlet weak var tableView: UITableView!
  @IBOutlet weak var searchViewTopConstraint: NSLayoutConstraint!
  @IBOutlet weak var searchView: UIView!
  @IBOutlet weak var searchViewVisualEffect: UIVisualEffectView!
  @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
  
  let viewModel = CityViewModel()
  var searchViewHeightConstraint: NSLayoutConstraint?
  let defaultSearchViewHeight: CGFloat = 50
  var currentScrollOffset: CGFloat = 0
  var currentCityList: [City] = []
  var largestSafeArea: CGFloat = 0 // Keep track of the largest top safe area size for determining the height of the search view when rotating
  
  var mapViewController: CityMapViewController!
  
  override func viewDidLoad() {
    super.viewDidLoad()
    
    tableView.contentInset = UIEdgeInsets(top: view.safeAreaInsets.top + defaultSearchViewHeight, left: 0, bottom: view.safeAreaInsets.bottom, right: 0)
    tableView.scrollIndicatorInsets = tableView.contentInset
    tableView.register(UINib(nibName: "CityCell", bundle: nil), forCellReuseIdentifier: "CityCellIdentifier")
    
    if #available(iOS 13, *) {
      searchViewVisualEffect.effect = UIBlurEffect(style: .systemUltraThinMaterial)
      activityIndicator.style = .large
    }
    
    searchView.layer.cornerRadius = 10
    
    loadCityData()
  }
  
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    
    largestSafeArea = largestSafeArea > view.safeAreaInsets.top ? largestSafeArea : view.safeAreaInsets.top
    
    if searchViewHeightConstraint == nil {
      searchViewHeightConstraint = searchView.heightAnchor.constraint(equalToConstant: defaultSearchViewHeight + (UIDevice.current.orientation.isLandscape ? 0 : largestSafeArea))
      searchView.addConstraint(searchViewHeightConstraint!)
    }
    
    tableView.reloadData()
  }
  
  func loadCityData() {
    // Load city data asynchronously and then unhide the loading screen
    DispatchQueue.global().async {
      self.viewModel.loadCityData()
      DispatchQueue.main.async {
        self.currentCityList = self.viewModel.getCities()
        self.tableView.reloadData()
        UIView.animate(withDuration: 0.3) {
          self.loadingView.alpha = 0
        } completion: { success in
          self.loadingView.isHidden = true
        }
      }
    }
  }
  
  override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
    super.viewWillTransition(to: size, with: coordinator)
    
    largestSafeArea = largestSafeArea > view.safeAreaInsets.top ? largestSafeArea : view.safeAreaInsets.top
    
    // If we are landscape we do not need to take the safe area into account for the height of the seach view
    if UIDevice.current.orientation.isLandscape {
      searchViewHeightConstraint?.constant = defaultSearchViewHeight
    } else {
      searchViewHeightConstraint?.constant = defaultSearchViewHeight + largestSafeArea
      tableView.contentOffset = CGPoint(x: 0, y: 0) // Avoids odd behaviour with scrollviews when rotating
    }
    
    searchViewTopConstraint.constant = 0 // Reset the search views positon on rotation
  }
  
  // MARK: - UITextFieldDelegate
  
  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
    
    if string.isEmpty, let text = textField.text { // Backspace pressed
      currentCityList = viewModel.getCities(withPrefix: text[0..<(text.count-1)]) // Truncate the last char off the string
    } else {
      let string = string.trimmingCharacters(in: NSCharacterSet.whitespacesAndNewlines)
      currentCityList = viewModel.getCities(withPrefix: "\(textField.text ?? "")\(string)") // Add the character that changed to the string
    }
    
    tableView.reloadData()
    
    return true
  }
  
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
  
  // MARK: - UITableView DataSource
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return currentCityList.count
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    
    if let cell = tableView.dequeueReusableCell(withIdentifier: "CityCellIdentifier") as? CityCell {
      cell.configure(with: currentCityList[indexPath.row])
      
      return cell
    }
    
    return UITableViewCell()
  }
  
  // MARK: - UITableView Delegate
  
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    if mapViewController == nil {
      mapViewController = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapView") as! CityMapViewController)
    }
    
    mapViewController.setCity(currentCityList[indexPath.row])
    
    if UIDevice.current.userInterfaceIdiom == .phone {
      show(mapViewController, sender: self)
    }
  }
  
  // MARK: - UIScrollViewDelegate
  
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
    
    let offset = scrollView.contentOffset.y + scrollView.contentInset.top + view.safeAreaInsets.top
    
    if offset >= 0 {
      // Determine how much to move the searchView up or down based on how much the user has scrolled
      let scrollAmount = (currentScrollOffset - offset)
      searchViewTopConstraint.constant = searchViewTopConstraint.constant + scrollAmount
      
      // Dont move more than the height of the searchView itself
      if searchViewTopConstraint.constant <= -(searchViewHeightConstraint?.constant ?? defaultSearchViewHeight) {
        searchViewTopConstraint.constant = -(searchViewHeightConstraint?.constant ?? defaultSearchViewHeight)
      }
      
      if searchViewTopConstraint.constant >= 0 {
        searchViewTopConstraint.constant = 0
      }
    } else {
      searchViewTopConstraint.constant = 0
    }
    
    currentScrollOffset = offset
  }
}

class CityCell: UITableViewCell {
  @IBOutlet weak var titleLabel: UILabel!
  @IBOutlet weak var subtitleLabel: UILabel!
  
  func configure(with city: City) {
    titleLabel.text = "\(city.name), \(city.country)"
    subtitleLabel.text = "lat: \(city.coordinates.lat), lon: \(city.coordinates.lon)"
  }
}
