//
//  CitySplitViewController.swift
//  Backbase Test
//
//  Created by Jacques Questiaux on 2020/09/20.
//  Copyright © 2020 Jacques Questiaux. All rights reserved.
//

import UIKit

class CitySplitViewController: UIViewController {

  @IBOutlet weak var masterView: UIView!
  @IBOutlet weak var detailView: UIView!
  
  var cityList: CityListViewController!
  var mapView: CityMapViewController!
  
  override func viewDidLoad() {
    super.viewDidLoad()

    cityList = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "cityList") as! CityListViewController)
    mapView = (UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "mapView") as! CityMapViewController)
    
    cityList.mapViewController = mapView
    
    masterView.addSubview(cityList.view)
    detailView.addSubview(mapView.view)
    
    cityList.didMove(toParent: self)
    mapView.didMove(toParent: self)
    
    cityList.view.bindFrameToSuperviewBounds()
    mapView.view.bindFrameToSuperviewBounds()
  }
}

extension UIView {
  
  func bindFrameToSuperviewBounds(top: CGFloat = 0, bottom: CGFloat = 0, left: CGFloat = 0, right: CGFloat = 0) {
    guard let superview = self.superview else {
      assert(false, "Error! `superview` was nil – call `addSubview(view: UIView)` before calling `bindFrameToSuperviewBounds()` to fix this.")
      return
    }
    
    self.translatesAutoresizingMaskIntoConstraints = false
    superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-(\(left))-[subview]-(\(right))-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
    superview.addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-(\(top))-[subview]-(\(bottom))-|", options: .directionLeadingToTrailing, metrics: nil, views: ["subview": self]))
  }
  
  func bindFrameToSuperviewBounds(padding: CGFloat = 0) {
    bindFrameToSuperviewBounds(top: padding, bottom: padding, left: padding, right: padding)
  }
}
