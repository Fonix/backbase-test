//
//  CityDataSource.swift
//  Backbase Test
//
//  Created by Jacques Questiaux on 2020/09/17.
//  Copyright © 2020 Jacques Questiaux. All rights reserved.
//

import Foundation

struct CityDataSource {
  
  var fileReader: FileReader
  
  init(fileReader: FileReader = FileReader()) {
    self.fileReader = fileReader
  }
  
  func sortedCityData(from filename: String) throws -> [City] {
    // Return sorted list of cities (case insensitive), with format "city name, country code"
    return try getCityData(from: filename).sorted(by: { (city1, city2) -> Bool in
      "\(city1.name.lowercased()), \(city1.country.lowercased())"
        .compare("\(city2.name.lowercased()), \(city2.country.lowercased())") == .orderedAscending
    })
  }
  
  private func getCityData(from filename: String) throws -> [City] {
    return try fileReader.readData(from: filename, forType: [City].self)
  }
}
