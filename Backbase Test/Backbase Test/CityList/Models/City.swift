//
//  City.swift
//  Backbase Test
//
//  Created by Jacques Questiaux on 2020/09/15.
//  Copyright © 2020 Jacques Questiaux. All rights reserved.
//

import Foundation

struct City: Decodable, Equatable {
  
  let id: Int
  let country: String
  let name: String
  let coordinates: Coordinate
  
  private enum CodingKeys : String, CodingKey {
    case id = "_id"
    case country
    case name
    case coordinates = "coord"
  }
  
  struct Coordinate: Decodable {
    let lat: Double
    let lon: Double
  }
  
  static func == (lhs: City, rhs: City) -> Bool {
    return lhs.id == rhs.id
  }
}
