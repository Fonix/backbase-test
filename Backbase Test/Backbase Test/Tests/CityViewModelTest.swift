//
//  CityViewModelTest.swift
//  Backbase TestTests
//
//  Created by Jacques Questiaux on 2020/09/17.
//  Copyright © 2020 Jacques Questiaux. All rights reserved.
//

import XCTest
@testable import Backbase_Test

class CityViewModelTest: XCTestCase {

  var sut: CityViewModel!
  let dataSource = CityDataSource(fileReader: FileReader(bundle: Bundle(for: CityViewModelTest.self)))
  
  override func setUp() {
    super.setUp()
    sut = CityViewModel(dataSource: dataSource)
  }

  override func tearDown() {
    sut = nil
    super.tearDown()
  }

  func testLoadData() {
    sut.loadCityData(from: "cities_subset")
    XCTAssert(sut.getCities().count == 500)
  }
  
  // Test if it gracefully handles corrupted json data
  func testLoadDataFail() {
    sut.loadCityData(from: "cities_json_error")
    XCTAssert(sut.getCities().count == 0)
  }

  func testSingleSearch() {
    
    let expectedResult = City(id: 370366,
                              country: "SD",
                              name: "Massa",
                              coordinates: City.Coordinate(lat: 10.98333,
                                                           lon: 29.466669))
    
    sut.loadCityData(from: "cities_subset")
    
    self.measure {
      let cities = sut.getCities(withPrefix: "Massa")
      if cities.count == 1 {
        XCTAssert(expectedResult == cities.first!)
      } else {
        XCTFail("Incorrect number of cities returned from search")
      }
    }
  }
  
  func testCaseInsensivity() {
    let expectedResult = [City(id: 3007202,
                               country: "FR",
                               name: "La Portanière",
                               coordinates: City.Coordinate(lat: 43.248611,
                                                            lon: 6.17341)),
                          City(id: 6356206,
                               country: "ES",
                               name: "la Roca del Vallès",
                               coordinates: City.Coordinate(lat: 41.601181,
                                                            lon: 2.32741))]
    
    sut.loadCityData(from: "cities_subset")
    
    self.measure {
      let cities = sut.getCities(withPrefix: "la ")
      if cities.count == 2 {
        for i in 0..<cities.count {
          let expectedCity = expectedResult[i]
          let city = cities[i]
          XCTAssert(expectedCity == city)
        }
      } else {
        XCTFail("Incorrect number of cities returned from search")
      }
    }
  }
  
  func testSingleSearchLowerBoundary() {
    
    let expectedResult = City(id: 7292321,
                              country: "GB",
                              name: "Aberystwyth",
                              coordinates: City.Coordinate(lat: -4.07708,
                                                           lon: 52.41227))
    
    sut.loadCityData(from: "cities_subset")
    
    self.measure {
      let cities = sut.getCities(withPrefix: "Ab")
      if cities.count == 1 {
        XCTAssert(expectedResult == cities.first!)
      } else {
        XCTFail("Incorrect number of cities returned from search")
      }
    }
  }
  
  func testSingleSearchUpperBoundary() {
    
    let expectedResult = City(id: 295582,
                              country: "IL",
                              name: "‘Azriqam",
                              coordinates: City.Coordinate(lat: 31.75,
                                                           lon: 34.700001))
    
    sut.loadCityData(from: "cities_subset")
    
    self.measure {
      let cities = sut.getCities(withPrefix: "‘")
      if cities.count == 1 {
        XCTAssert(expectedResult == cities.first!)
      } else {
        XCTFail("Incorrect number of cities returned from search")
      }
    }
  }
  
  func testMultiSearch() {
    
    let expectedResult = [City(id: 154733,
                               country: "TZ",
                               name: "Masama",
                               coordinates: City.Coordinate(lat: -3.23333,
                                                            lon: 37.183331)),
                          City(id: 370366,
                               country: "SD",
                               name: "Massa",
                               coordinates: City.Coordinate(lat: 10.98333,
                                                            lon: 29.466669))]
    
    sut.loadCityData(from: "cities_subset")
    
    self.measure {
      let cities = sut.getCities(withPrefix: "Mas")
      if cities.count == 2 {
        for i in 0..<cities.count {
          let expectedCity = expectedResult[i]
          let city = cities[i]
          XCTAssert(expectedCity == city)
        }
      } else {
        XCTFail("Incorrect number of cities returned from search")
      }
    }
  }
  
  func testLargeResult() {
    sut.loadCityData(from: "cities_subset")
    XCTAssert(sut.getCities(withPrefix: "A").count == 17)
    XCTAssert(sut.getCities(withPrefix: "T").count == 23)
    XCTAssert(sut.getCities(withPrefix: "‘").count == 1)
  }
  
  func testNoResults() {
    sut.loadCityData(from: "cities_subset")
    XCTAssert(sut.getCities(withPrefix: "AAAAA").count == 0) // Get no results from lower boundary
    XCTAssert(sut.getCities(withPrefix: "‘‘‘").count == 0) // Get no results from upper boundary
    XCTAssert(sut.getCities(withPrefix: "kdjfgh").count == 0) // Get no results from somewhere in the middle
  }
  
  func testSearchSingleItemInFile() {
    sut.loadCityData(from: "cities_single")
    XCTAssert(sut.getCities(withPrefix: "Hurzuf").count == 1)
    XCTAssert(sut.getCities(withPrefix: "aaaa").count == 0)
  }
  
  func testSearchNoItemsInFile() {
    sut.loadCityData(from: "cities_empty")
    XCTAssert(sut.getCities(withPrefix: "Hurzuf").count == 0)
  }
  
  func testEquality() {
    let expectedResult = City(id: 295582,
                              country: "IL",
                              name: "‘Azriqam",
                              coordinates: City.Coordinate(lat: 31.75,
                                                           lon: 34.700001))
    
    let expectedResultEqual = City(id: 295582,
                                   country: "IL",
                                   name: "‘Azriqam",
                                   coordinates: City.Coordinate(lat: 31.75,
                                                            lon: 34.700001))
    
    XCTAssertTrue(expectedResult == expectedResultEqual)
    
    
    let expectedResultUnequal = City(id: 295583,
                                     country: "IL",
                                     name: "‘Azriqam",
                                     coordinates: City.Coordinate(lat: 31.75,
                                                                  lon: 34.700001))
    
    XCTAssertTrue(expectedResult != expectedResultUnequal)
  }
}
